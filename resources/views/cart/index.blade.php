@extends('layouts.app')

@section('content')
    <h2>Cart</h2>

        <table class="table">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Price</th>
                    <th>Quantity</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($cartItems as $item)
                    <tr>
                        <td scope="row">{{ $item->name }}</td>
                        <td>
                            {{Cart::session(auth()->id())->get($item->id)->getPriceSum()}}
                        </td>
                        <td></td>
                        <td>
                            <form action="{{ url('/cart/update', $item->id) }}">
                                <input type="number" value="{{ $item->quantity }}">
                                <input type="submit" value="save">
                            </form>
                        </td>
                        <td>
                            <a href="{{ url('/cart/destroy', $item->id) }}">Delete</a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>

        <h3>
            Total Price : {{ Cart::session(auth()->id())->getTotal() }}
        </h3>

        <a class="btn btn-primary" href="{{ route('cart.checkout')}}" role="button">Proceed To Checkout</a>
@endsection
