@extends('layouts.app')

@section('content')
<h2>Submit Your Shop Here</h2>

<form action="{{ route('shop.store') }}" method="post">

    @csrf

    <div class="form-group">
        <label for="name">Name of Shop</label>
        <input type="text" class="form-control" name="name" id="" aria-describedby="helpId" placeholder="">
    </div>

    <div class="form-group">
        <label for="description">Description</label>
        <textarea name="description" id="" class="form-control" rows="3"></textarea>
    </div>

    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection
