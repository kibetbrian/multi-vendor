@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <h2>Products</h2>

        <div class="row">

            @foreach ($products as $product)

            <div class="col-4">
                <div class="card">
                    <img class="card-img-top" src="{{ asset('default.jpg') }}" alt="Card image cap">
                    <div class="card-body">
                        <h4 class="card-title">{{ $product->name }}</h4>
                        <p class="card-text">{{ $product->description }}</p>
                        <h3>{{ $product->price }}</h3>
                    </div>
                    <div class="card-body">
                        <a href="{{ url('/add-to-cart', $product->id) }}" class="card-link">Add To Cart</a>
                    </div>
                </div>
            </div>

            @endforeach

        </div>
    </div>
</div>
@endsection
