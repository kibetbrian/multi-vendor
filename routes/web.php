<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ShopController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::redirect('/', '/home');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

// Cart
Route::get('/add-to-cart/{product}', [App\Http\Controllers\CartController::class, 'add'])->name('add.cart')->middleware('auth');
Route::get('/cart', [App\Http\Controllers\CartController::class, 'index'])->name('cart.index')->middleware('auth');
Route::get('/cart/destroy/{itemId}', [App\Http\Controllers\CartController::class, 'destroy'])->name('cart.destroy')->middleware('auth');
Route::get('/cart/update/{itemId}', [App\Http\Controllers\CartController::class, 'update'])->name('cart.update')->middleware('auth');
Route::get('/checkout', [App\Http\Controllers\CartController::class, 'checkout'])->name('cart.checkout')->middleware('auth');

//  Order
Route::post('/orders', [App\Http\Controllers\OrderController::class, 'store'])->name('orders.store')->middleware('auth');

//Shops
Route::resource('shop', ShopController::class)->middleware('auth');
//Route::get('shop', ShopController::class, 'create')->name('shop.create');

//Paypal
Route::get('/paypal/checkout', [App\Http\Controllers\PayPalController::class, 'getExpressCheckout'])->name('paypal.checkout')->middleware('auth');
Route::get('/paypal/checkout-success', [App\Http\Controllers\PayPalController::class, 'getExpressCheckoutSuccess'])->name('paypal.success')->middleware('auth');
Route::get('/paypal/checkout-cancel', [App\Http\Controllers\PayPalController::class, 'getExpressCheckoutCancel'])->name('paypal.cancel')->middleware('auth');


Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
