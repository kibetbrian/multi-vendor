<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        \DB::table('users')->delete();

        \DB::table('users')->insert(array (
            0 =>
            array (
                'avatar' => 'users/default.png',
                'created_at' => '2021-07-21 10:54:31',
                'email' => 'admin@brian.com',
                'email_verified_at' => '2021-07-21 10:54:31',
                'id' => 1,
                'name' => 'Admin',
                'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',
                'remember_token' => 'LX8CWfhoQO',
                'role_id' => 1,
                'settings' => NULL,
                'updated_at' => '2021-07-21 10:54:31',
            ),
            1 =>
            array (
                'avatar' => 'users/default.png',
                'created_at' => '2021-07-21 10:54:31',
                'email' => 'jenkins.rosanna@example.com',
                'email_verified_at' => '2021-07-21 10:54:31',
                'id' => 2,
                'name' => 'Fiona Cormier',
                'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',
                'remember_token' => 'CqprFFmZ6E',
                'role_id' => 2,
                'settings' => NULL,
                'updated_at' => '2021-07-21 10:54:31',
            ),

            2 =>
            array (
                'avatar' => 'users/default.png',
                'created_at' => '2021-07-21 10:56:29',
                'email' => 'hayes.durward@example.com',
                'email_verified_at' => '2021-07-21 10:56:29',
                'id' => 20,
                'name' => 'Prof. Waino Hickle Sr.',
                'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',
                'remember_token' => 'CDmW4wgUvZ',
                'role_id' => 2,
                'settings' => NULL,
                'updated_at' => '2021-07-21 10:56:29',
            ),
            3 =>
            array (
                'avatar' => 'users/default.png',
                'created_at' => '2021-07-21 13:55:13',
                'email' => 'kimutaibrian9@gmail.com',
                'email_verified_at' => NULL,
                'id' => 21,
                'name' => 'Brian Kiptanui',
                'password' => '$2y$10$axkKutWYXb2K0mVhyYKzLe9yz/YCMI5fIElpkYYBJ3xYzYZFm0JBO',
                'remember_token' => 'Qb9C8sEU0KEyi2dxRnnUG8UaTfQ4h7U0mmJMPgylaA0phvAQElKMBEXLFHUN',
                'role_id' => 2,
                'settings' => NULL,
                'updated_at' => '2021-07-23 12:04:51',
            ),
        ));


    }
}
