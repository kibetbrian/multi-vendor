<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('permissions')->delete();
        
        \DB::table('permissions')->insert(array (
            0 => 
            array (
                'created_at' => '2021-07-23 11:38:19',
                'id' => 1,
                'key' => 'browse_admin',
                'table_name' => NULL,
                'updated_at' => '2021-07-23 11:38:19',
            ),
            1 => 
            array (
                'created_at' => '2021-07-23 11:38:19',
                'id' => 2,
                'key' => 'browse_bread',
                'table_name' => NULL,
                'updated_at' => '2021-07-23 11:38:19',
            ),
            2 => 
            array (
                'created_at' => '2021-07-23 11:38:19',
                'id' => 3,
                'key' => 'browse_database',
                'table_name' => NULL,
                'updated_at' => '2021-07-23 11:38:19',
            ),
            3 => 
            array (
                'created_at' => '2021-07-23 11:38:19',
                'id' => 4,
                'key' => 'browse_media',
                'table_name' => NULL,
                'updated_at' => '2021-07-23 11:38:19',
            ),
            4 => 
            array (
                'created_at' => '2021-07-23 11:38:19',
                'id' => 5,
                'key' => 'browse_compass',
                'table_name' => NULL,
                'updated_at' => '2021-07-23 11:38:19',
            ),
            5 => 
            array (
                'created_at' => '2021-07-23 11:38:19',
                'id' => 6,
                'key' => 'browse_menus',
                'table_name' => 'menus',
                'updated_at' => '2021-07-23 11:38:19',
            ),
            6 => 
            array (
                'created_at' => '2021-07-23 11:38:19',
                'id' => 7,
                'key' => 'read_menus',
                'table_name' => 'menus',
                'updated_at' => '2021-07-23 11:38:19',
            ),
            7 => 
            array (
                'created_at' => '2021-07-23 11:38:19',
                'id' => 8,
                'key' => 'edit_menus',
                'table_name' => 'menus',
                'updated_at' => '2021-07-23 11:38:19',
            ),
            8 => 
            array (
                'created_at' => '2021-07-23 11:38:19',
                'id' => 9,
                'key' => 'add_menus',
                'table_name' => 'menus',
                'updated_at' => '2021-07-23 11:38:19',
            ),
            9 => 
            array (
                'created_at' => '2021-07-23 11:38:19',
                'id' => 10,
                'key' => 'delete_menus',
                'table_name' => 'menus',
                'updated_at' => '2021-07-23 11:38:19',
            ),
            10 => 
            array (
                'created_at' => '2021-07-23 11:38:19',
                'id' => 11,
                'key' => 'browse_roles',
                'table_name' => 'roles',
                'updated_at' => '2021-07-23 11:38:19',
            ),
            11 => 
            array (
                'created_at' => '2021-07-23 11:38:19',
                'id' => 12,
                'key' => 'read_roles',
                'table_name' => 'roles',
                'updated_at' => '2021-07-23 11:38:19',
            ),
            12 => 
            array (
                'created_at' => '2021-07-23 11:38:19',
                'id' => 13,
                'key' => 'edit_roles',
                'table_name' => 'roles',
                'updated_at' => '2021-07-23 11:38:19',
            ),
            13 => 
            array (
                'created_at' => '2021-07-23 11:38:19',
                'id' => 14,
                'key' => 'add_roles',
                'table_name' => 'roles',
                'updated_at' => '2021-07-23 11:38:19',
            ),
            14 => 
            array (
                'created_at' => '2021-07-23 11:38:19',
                'id' => 15,
                'key' => 'delete_roles',
                'table_name' => 'roles',
                'updated_at' => '2021-07-23 11:38:19',
            ),
            15 => 
            array (
                'created_at' => '2021-07-23 11:38:19',
                'id' => 16,
                'key' => 'browse_users',
                'table_name' => 'users',
                'updated_at' => '2021-07-23 11:38:19',
            ),
            16 => 
            array (
                'created_at' => '2021-07-23 11:38:19',
                'id' => 17,
                'key' => 'read_users',
                'table_name' => 'users',
                'updated_at' => '2021-07-23 11:38:19',
            ),
            17 => 
            array (
                'created_at' => '2021-07-23 11:38:19',
                'id' => 18,
                'key' => 'edit_users',
                'table_name' => 'users',
                'updated_at' => '2021-07-23 11:38:19',
            ),
            18 => 
            array (
                'created_at' => '2021-07-23 11:38:19',
                'id' => 19,
                'key' => 'add_users',
                'table_name' => 'users',
                'updated_at' => '2021-07-23 11:38:19',
            ),
            19 => 
            array (
                'created_at' => '2021-07-23 11:38:19',
                'id' => 20,
                'key' => 'delete_users',
                'table_name' => 'users',
                'updated_at' => '2021-07-23 11:38:19',
            ),
            20 => 
            array (
                'created_at' => '2021-07-23 11:38:19',
                'id' => 21,
                'key' => 'browse_settings',
                'table_name' => 'settings',
                'updated_at' => '2021-07-23 11:38:19',
            ),
            21 => 
            array (
                'created_at' => '2021-07-23 11:38:19',
                'id' => 22,
                'key' => 'read_settings',
                'table_name' => 'settings',
                'updated_at' => '2021-07-23 11:38:19',
            ),
            22 => 
            array (
                'created_at' => '2021-07-23 11:38:19',
                'id' => 23,
                'key' => 'edit_settings',
                'table_name' => 'settings',
                'updated_at' => '2021-07-23 11:38:19',
            ),
            23 => 
            array (
                'created_at' => '2021-07-23 11:38:19',
                'id' => 24,
                'key' => 'add_settings',
                'table_name' => 'settings',
                'updated_at' => '2021-07-23 11:38:19',
            ),
            24 => 
            array (
                'created_at' => '2021-07-23 11:38:19',
                'id' => 25,
                'key' => 'delete_settings',
                'table_name' => 'settings',
                'updated_at' => '2021-07-23 11:38:19',
            ),
            25 => 
            array (
                'created_at' => '2021-07-23 11:43:51',
                'id' => 26,
                'key' => 'browse_hooks',
                'table_name' => NULL,
                'updated_at' => '2021-07-23 11:43:51',
            ),
            26 => 
            array (
                'created_at' => '2021-07-23 11:49:38',
                'id' => 27,
                'key' => 'browse_categories',
                'table_name' => 'categories',
                'updated_at' => '2021-07-23 11:49:38',
            ),
            27 => 
            array (
                'created_at' => '2021-07-23 11:49:38',
                'id' => 28,
                'key' => 'read_categories',
                'table_name' => 'categories',
                'updated_at' => '2021-07-23 11:49:38',
            ),
            28 => 
            array (
                'created_at' => '2021-07-23 11:49:38',
                'id' => 29,
                'key' => 'edit_categories',
                'table_name' => 'categories',
                'updated_at' => '2021-07-23 11:49:38',
            ),
            29 => 
            array (
                'created_at' => '2021-07-23 11:49:38',
                'id' => 30,
                'key' => 'add_categories',
                'table_name' => 'categories',
                'updated_at' => '2021-07-23 11:49:38',
            ),
            30 => 
            array (
                'created_at' => '2021-07-23 11:49:38',
                'id' => 31,
                'key' => 'delete_categories',
                'table_name' => 'categories',
                'updated_at' => '2021-07-23 11:49:38',
            ),
            31 => 
            array (
                'created_at' => '2021-07-23 11:52:00',
                'id' => 32,
                'key' => 'browse_posts',
                'table_name' => 'posts',
                'updated_at' => '2021-07-23 11:52:00',
            ),
            32 => 
            array (
                'created_at' => '2021-07-23 11:52:00',
                'id' => 33,
                'key' => 'read_posts',
                'table_name' => 'posts',
                'updated_at' => '2021-07-23 11:52:00',
            ),
            33 => 
            array (
                'created_at' => '2021-07-23 11:52:00',
                'id' => 34,
                'key' => 'edit_posts',
                'table_name' => 'posts',
                'updated_at' => '2021-07-23 11:52:00',
            ),
            34 => 
            array (
                'created_at' => '2021-07-23 11:52:00',
                'id' => 35,
                'key' => 'add_posts',
                'table_name' => 'posts',
                'updated_at' => '2021-07-23 11:52:00',
            ),
            35 => 
            array (
                'created_at' => '2021-07-23 11:52:00',
                'id' => 36,
                'key' => 'delete_posts',
                'table_name' => 'posts',
                'updated_at' => '2021-07-23 11:52:00',
            ),
            36 => 
            array (
                'created_at' => '2021-07-23 11:52:00',
                'id' => 37,
                'key' => 'browse_pages',
                'table_name' => 'pages',
                'updated_at' => '2021-07-23 11:52:00',
            ),
            37 => 
            array (
                'created_at' => '2021-07-23 11:52:00',
                'id' => 38,
                'key' => 'read_pages',
                'table_name' => 'pages',
                'updated_at' => '2021-07-23 11:52:00',
            ),
            38 => 
            array (
                'created_at' => '2021-07-23 11:52:00',
                'id' => 39,
                'key' => 'edit_pages',
                'table_name' => 'pages',
                'updated_at' => '2021-07-23 11:52:00',
            ),
            39 => 
            array (
                'created_at' => '2021-07-23 11:52:00',
                'id' => 40,
                'key' => 'add_pages',
                'table_name' => 'pages',
                'updated_at' => '2021-07-23 11:52:00',
            ),
            40 => 
            array (
                'created_at' => '2021-07-23 11:52:00',
                'id' => 41,
                'key' => 'delete_pages',
                'table_name' => 'pages',
                'updated_at' => '2021-07-23 11:52:00',
            ),
            41 => 
            array (
                'created_at' => '2021-07-23 12:50:25',
                'id' => 42,
                'key' => 'browse_orders',
                'table_name' => 'orders',
                'updated_at' => '2021-07-23 12:50:25',
            ),
            42 => 
            array (
                'created_at' => '2021-07-23 12:50:25',
                'id' => 43,
                'key' => 'read_orders',
                'table_name' => 'orders',
                'updated_at' => '2021-07-23 12:50:25',
            ),
            43 => 
            array (
                'created_at' => '2021-07-23 12:50:25',
                'id' => 44,
                'key' => 'edit_orders',
                'table_name' => 'orders',
                'updated_at' => '2021-07-23 12:50:25',
            ),
            44 => 
            array (
                'created_at' => '2021-07-23 12:50:25',
                'id' => 45,
                'key' => 'add_orders',
                'table_name' => 'orders',
                'updated_at' => '2021-07-23 12:50:25',
            ),
            45 => 
            array (
                'created_at' => '2021-07-23 12:50:25',
                'id' => 46,
                'key' => 'delete_orders',
                'table_name' => 'orders',
                'updated_at' => '2021-07-23 12:50:25',
            ),
        ));
        
        
    }
}